resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "key_pair" {
  key_name = "${var.name}"
  public_key = "${tls_private_key.ssh_key.public_key_openssh}"
}

resource "aws_s3_bucket_object" "upload_key_to_s3" {
  bucket = "${var.private_keys_bucket_name}"
  key = "${var.name}.pem"
  content = "${tls_private_key.ssh_key.private_key_pem}"
}