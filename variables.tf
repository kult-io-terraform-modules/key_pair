variable "name" {
  description = "A name for the private key"
}

variable "private_keys_bucket_name" {
  description = "A bucket name used for keeping the private keys"
}